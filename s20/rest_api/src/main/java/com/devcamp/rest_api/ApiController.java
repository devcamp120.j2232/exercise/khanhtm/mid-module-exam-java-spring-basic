package com.devcamp.rest_api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ApiController {
    @GetMapping("/reverse")
    public String reverse(@RequestParam(value = "reverse", defaultValue = "abc") String reverse) {
        String str = reverse, result = "";
        char ch;
        for (int i = 0; i < str.length(); i++) {
            ch = str.charAt(i); // extracts each character
            result = ch + result; // adds each character in front of the existing string
        }
        return result;

    }

    @GetMapping("/palindrome")
    public boolean palindrome(@RequestParam(value = "palindrome", defaultValue = "aba") String palindrome) {
        String str = palindrome;
        String rev = "";
        // Initializing a new boolean variable for the
        // answer
        boolean ans = false;

        for (int i = str.length() - 1; i >= 0; i--) {
            rev = rev + str.charAt(i);
        }
        // Checking if both the strings are equal
        if (str.equals(rev)) {
            ans = true;
        }
        return ans;
    }

    @GetMapping("/RemoveDuplicates")
    public String RemoveDuplicates(
            @RequestParam(value = "RemoveDuplicates", defaultValue = "bananas") String RemoveDuplicates) {
        String result = "";
        for (char c : RemoveDuplicates.toCharArray()) {
            result += result.contains(c + "")
                    ? ""
                    : c;
        }
        return result;
    }

    @GetMapping("/appendstring")
    public String appendstring(
            @RequestParam(value = "str1", defaultValue = "welcome") String string1,
            @RequestParam(value = "str2", defaultValue = "home") String string2) {
                String st1 = string1;
                String st2 = string2;
        if (st1.length() == st2.length())
            return st1 + st2;
        if (st1.length() > st2.length()) {
            int diff = st1.length() - st2.length();
            return st1.substring(diff, st1.length()) + st2;
        } else {
            int diff = st2.length() - st1.length();
            return st1 + st2.substring(diff, st2.length());
        }

    }
}
