package com.example.demo.Controller;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Classroom;
import com.example.demo.Service.ClassroomService;


@RestController
public class ClassroomController {
    
    @GetMapping("/classroomAll")
    public ArrayList<Classroom> getSchoolAll(){
        ArrayList<Classroom> classes = ClassroomService.getClassAll();
        return classes;
    }
    
    @GetMapping("/classroom-noNumber")
    public ArrayList<Classroom> getClassroomInfo(@RequestParam("noNumber") int noNumber) {
            ArrayList<Classroom> classes = ClassroomService.getClassAll();
        
            ArrayList <Classroom> classFind = new ArrayList<Classroom>();
            for(Classroom classroom : classes) {
                if(classroom.getNoStudent() > noNumber) {
                    classFind.add(classroom);
                }
            }
            return classFind;
        }    
    }
