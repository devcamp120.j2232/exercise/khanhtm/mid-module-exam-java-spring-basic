package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.School;
@Service
public class SchoolService {
    
    public static ArrayList<School> getSchoolList() {
        ArrayList<School> schoolList = new ArrayList<School>();

        School TieuHoc = new School(1,"Tiểu Học", "Lê Văn Sỹ QTB" , ClassroomService.getClassTieuHoc());
        School TrungHoc = new School(2,"Trung Học", "Điện Biên Phủ Q10" , ClassroomService.getClassTrungHoc());
        School PhoThong = new School(3,"Phổ Thông", "Đồng Khởi Q1" , ClassroomService.getClassPhoThong());

        schoolList.add(TieuHoc);
        schoolList.add(TrungHoc);
        schoolList.add(PhoThong);

        return schoolList;
    }

    
}
